package com.example.newsfeed;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.squareup.moshi.Moshi;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;


public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    EditText keywordField;
    EditText numberField;
    Button searchBtn, btnAboutUs;
    ToggleButton toggleOption;
    final String APIKey = "91a6a3fa95ed44169006ff7a64cfcc8e";
    Adapter adapter;
    List<Article> articles = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        swipeRefreshLayout = findViewById(R.id.swipeRefresh);
        recyclerView = findViewById(R.id.recyclerView);
        toggleOption = findViewById(R.id.toggleOption);
        numberField = findViewById(R.id.NumberField);
        keywordField = findViewById(R.id.KeywordField);
        searchBtn = findViewById(R.id.searchBtn);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        Moshi moshi = new Moshi.Builder().build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://newsapi.org/v2/")
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .build();
        final InterfaceAPI api = retrofit.create(InterfaceAPI.class); //retrofit sucelje za dohvat podataka

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getNews(keywordField.getText().toString(),APIKey,api);
            }
        });

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    getNews(keywordField.getText().toString(),APIKey, api);
            }
        });

        getNews(keywordField.getText().toString(),APIKey,api);

    }


    public void getNews(String query , String apiKey, InterfaceAPI api){
        int resultNumber = 10; //default broj rezultata:10
        if(!numberField.getText().toString().equals("")) resultNumber = Integer.parseInt(numberField.getText().toString()); //korisnikov unos
        if(resultNumber>100) resultNumber=100; //maksimum 100 rezultata
        swipeRefreshLayout.setRefreshing(true);
        Call<Articles> call;
        if (toggleOption.isChecked() && !query.equals("")){ //headlines ili everything
            call= api.getEverything(query ,resultNumber, apiKey); //everything se poziva samo ako ima kljucnih rijeci upisanih, jer bez njega odgovor ne stize(pretpostavljam zbog prevelikog broja podataka)
        }else{
            call= api.getHeadlines("us", query, resultNumber, apiKey); //headlines
        }

        call.enqueue(new Callback<Articles>() { //asinkrono pozivanje
            @Override
            public void onResponse(Call<Articles> call, Response<Articles> response) { //procesiranje odgovora
                if (response.isSuccessful() && response.body().getArticles() != null){
                    swipeRefreshLayout.setRefreshing(false);
                    articles.clear();
                    articles = response.body().getArticles();
                    adapter = new Adapter(MainActivity.this,articles); //adapter za RecyclerView
                    recyclerView.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<Articles> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                Toast.makeText(MainActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}



