package com.example.newsfeed;

import com.squareup.moshi.Json;

//JSON model za source dio
public class Source {
    @Json(name="id") private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Json(name="name") private String name;
}
