package com.example.newsfeed;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
//retrofit sucelje za dohvat podataka
public interface InterfaceAPI {
    @GET("top-headlines")
    Call<Articles> getHeadlines(
            @Query("country") String country,
            @Query("q") String query,
            @Query("pageSize") int pageSize,
            @Query("apiKey") String apiKey
    );

    @GET("everything")
    Call<Articles> getEverything(
            @Query("q") String query,
            @Query("pageSize") int pageSize,
            @Query("apiKey") String apiKey
            );


}
