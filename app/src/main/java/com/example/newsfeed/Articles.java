package com.example.newsfeed;

import com.squareup.moshi.Json;

import java.util.List;
//razred za JSON model koji predstavlja cijeli odgovor
public class Articles {

    @Json(name = "status") private String status;
    @Json(name = "totalResults") private String totalResults;
    @Json(name = "articles") private List<Article> articles;
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(String totalResults) {
        this.totalResults = totalResults;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

}
